# How To Manage Drives, Create Partitions, and Mount Them

When it comes to drives on OpenBSD it is important to remember that OpenBSD by default will not utilize the rest of a disk beyond 356 GB. So when installing OpenBSD if you used the default partition layout and your drive is bigger than 356 GB you will want to create other partitions on the drive and mount them at places like ~/Videos or do a manual partition layout during install. 

It's your system so it's up to you how you set it up and there isn't a right or wrong way for how you like your partition scheme.

## Find Drives Attached

This is what can really throw some people off who are coming to OpenBSD from Linux - there's no 'lsblk'. Instead you can check the devices connected to the system using:

```
# sysctl hw.disknames
```

### Alternative Method

You may also use this command; however, please keep in mind - dmesg will show every device that *has* been connected. It is like a log of everything that has happened on the system. **So unplugging a USB device then running this will show the drive as still there. **

```
# dmesg | egrep '^([cswf]d). '
```

## Check Information About Drive

Replace the name of the drive with the desired one.

```
# disklabel <sdX>
```

## Create New Partitions & Format Them

You only need to start disklabel with flag -E* and type a to add a partition, default will use all remaining space for the partition. Change the size option accordingly (you may use something like 20g to specify a 20 GB partition).

```
# disklabel -E <sdX>
Label editor (enter '?' for help at any prompt)
> a
partition: [m]
offset: [741349952]
size: [258863586]
FS type: [4.2BSD]
> w
> q
No label changes.
```

The new partition here is m. We can format it with:

```
# newfs /dev/rsd0m
```

## Adding To Your /etc/fstab

If you would like the partition to be mounted at boot than you will need to add the drive to your /etc/fstab. If this partition is an addition to the main boot drive anyway then you can copy your /home partition line and change the letter after the uuid and add the proper directory to mount to. Here the example directory is /data.

```
52fdd1ce48744600.m /data ffs rw,nodev,nosuid 1 2
```

It will be auto mounted at boot, you only need to create the folder /data.

```
# mkdir /data
# mount /dev/<sdX> /data
```

and /data is usable right now.

*Hope this guide has helped you, please share if it has!*
